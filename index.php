<?php
// https://jwt.io/

require_once 'includes/config.php';
require_once 'classes/JWT.php';

// Cree le header
$header = [
    'typ' => 'jwt',
    'alg' => 'HS256'
];

// Cree le contenu (payload)
$payload = [
    'user_id' => 123,
    'roles' => [
        'ROLE_ADMIN',
        'ROLE_USER'
    ],
    'email' => 'contact@demo.fr'
];

$jwt = new JWT();
$token = $jwt->generate($header, $payload , SECRET);
//$token = $jwt->generate($header, $payload , SECRET, 60);

echo $token;